//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: exampleBetaPlus.cc 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file exampleBetaPlus.cc
/// \brief Main program of the BetaPlus example

#include "BetaPlusDetectorConstruction.hh"
#include "BetaPlusPrimaryGeneratorAction.hh"
#include "BetaPlusRunAction.hh"
#include "BetaPlusEventAction.hh"
#include "BetaPlusSteppingAction.hh"

#include "G4StepLimiterPhysics.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "QGSP_BIC.hh"
#include "QGSP_BIC_HP.hh"
#include "QGSP_INCLXX.hh"
#include "QGSP_INCLXX_HP.hh"
#include "BetaPlusQGSP_QMD.hh"
#include "BetaPlusQGSP_QMD_HP.hh"

#include "Randomize.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc,char** argv)
{
  if(argc==1) {
    G4cerr << "Syntax error: betaPlus <list> [macro]\nPossible list values:\n  QGSP_BIC\n  QGSP_BIC_HP\n  QGSP_INCLXX\n  QGSP_INCLXX_HP\n  QGSP_QMD\n  QGSP_QMD_HP" << G4endl;
    return 1;
  }
  std::string argument(argv[1]);

  // Choose the Random engine

  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  
  // Construct the default run manager
  
  G4RunManager * runManager = new G4RunManager;

  // Set mandatory initialization classes

  runManager->SetUserInitialization(new BetaPlusDetectorConstruction());

  G4VModularPhysicsList* physicsList;
  if(argument == "QGSP_BIC") {
    physicsList = new QGSP_BIC;
    G4cout << "Created QGSP_BIC physics list" << G4endl;
  } else if(argument == "QGSP_BIC_HP") {
    physicsList = new QGSP_BIC_HP;
    G4cout << "Created QGSP_BIC_HP physics list" << G4endl;
  } else if(argument == "QGSP_INCLXX") {
    physicsList = new QGSP_INCLXX;
    G4cout << "Created QGSP_INCLXX physics list" << G4endl;
  } else if(argument == "QGSP_INCLXX_HP") {
    physicsList = new QGSP_INCLXX_HP;
    G4cout << "Created QGSP_INCLXX_HP physics list" << G4endl;
  } else if(argument == "QGSP_QMD") {
    physicsList = new BetaPlusQGSP_QMD;
    G4cout << "Created BetaPlusQGSP_QMD physics list" << G4endl;
  } else if(argument == "QGSP_QMD_HP") {
    physicsList = new BetaPlusQGSP_QMD_HP;
    G4cout << "Created BetaPlusQGSP_QMD_HP physics list" << G4endl;
  } else {
    std::cerr << "Error: must provide a physics list as argument. Possible values:\n  QGSP_BIC\n  QGSP_BIC_HP\n  QGSP_INCLXX\n  QGSP_INCLXX_HP\n  QGSP_QMD\n  QGSP_QMD_HP" << std::endl;
    return 1;
  }
  physicsList->RegisterPhysics(new G4StepLimiterPhysics());
  runManager->SetUserInitialization(physicsList);
    
  // Set user action classes

  runManager->SetUserAction(new BetaPlusPrimaryGeneratorAction(4.*cm));
  BetaPlusRunAction *runAction = new BetaPlusRunAction(argument, 400);
  runManager->SetUserAction(runAction);
  runManager->SetUserAction(new BetaPlusEventAction());
  runManager->SetUserAction(new BetaPlusSteppingAction(runAction));
  
  // Initialize G4 kernel

  runManager->Initialize();
  
#ifdef G4VIS_USE
  // Initialize visualization
  G4VisManager* visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();
#endif

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  if (argc!=2)   // batch mode
    {
      G4String command = "/control/execute ";
      G4String fileName = argv[2];
      UImanager->ApplyCommand(command+fileName);
    }
  else
    {  // interactive mode : define UI session
#ifdef G4UI_USE
      G4UIExecutive* ui = new G4UIExecutive(argc-1, argv+1, "tcsh");
#ifdef G4VIS_USE
        UImanager->ApplyCommand("/control/execute init_vis.mac");
#else
        UImanager->ApplyCommand("/control/execute init.mac");
#endif
      if (ui->IsGUI())
         UImanager->ApplyCommand("/control/execute gui.mac");
      ui->SessionStart();
      delete ui;
#endif
    }

  // Job termination
  // Free the store: user actions, physics_list and detector_description are
  // owned and deleted by the run manager, so they should not be deleted
  // in the main() program !

#ifdef G4VIS_USE
  delete visManager;
#endif
  delete runManager;

  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....

//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: BetaPlusDetectorConstruction.hh 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file BetaPlusDetectorConstruction.hh
/// \brief Definition of the BetaPlusDetectorConstruction class

#ifndef BetaPlusDetectorConstruction_h
#define BetaPlusDetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;
class G4UserLimits;

class BetaPlusDetectorMessenger;
class BetaPlusMagneticField;

/// Detector construction class to define materials and geometry.

class BetaPlusDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    BetaPlusDetectorConstruction();
    virtual ~BetaPlusDetectorConstruction();

  public:
    virtual G4VPhysicalVolume* Construct();

    // Set methods
    void SetTargetMaterial (G4String );
    void SetMaxStep (G4double );
    void SetCheckOverlaps(G4bool );

  private:
    // methods
    void DefineMaterials();
    G4VPhysicalVolume* DefineVolumes();
  
    G4LogicalVolume*   fLogicTarget;     // pointer to the logical Target

    G4Material*        fTargetMaterial; // pointer to the target material

    G4UserLimits* fStepLimit;            // pointer to user step limits

    BetaPlusDetectorMessenger*  fMessenger;   // messenger
    
    G4bool  fCheckOverlaps; // option to activate checking of volumes overlaps 
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

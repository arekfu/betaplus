//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: BetaPlusRunAction.hh 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file BetaPlusRunAction.hh
/// \brief Definition of the BetaPlusRunAction class

#ifndef BetaPlusRunAction_h
#define BetaPlusRunAction_h 1

#include "CLHEP/Units/SystemOfUnits.h"
#include "G4UserRunAction.hh"
#include "G4ThreeVector.hh"
#include "Randomize.hh"
#include "globals.hh"
#include "TH2F.h"
#include "TProfile2D.h"
#include "TTree.h"
#include "TFile.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4Run;

/// Run action class

class BetaPlusRunAction : public G4UserRunAction
{
  public:
    BetaPlusRunAction(const G4String &aFileStem, const int bins=60);
    virtual ~BetaPlusRunAction();

    virtual void BeginOfRunAction(const G4Run* run);
    virtual void   EndOfRunAction(const G4Run* run);

    void FillStopping(const G4ThreeVector &position);
    void FillStoppingAll(const int A, const int Z, const G4ThreeVector &position, const bool betaPlus);
    void FillStoppingPrimary(const G4ThreeVector &position);
    void FillBragg(G4ThreeVector const &prePosition, G4ThreeVector const &postPosition, const double eDep);

  private:
    const G4String outputFileStem;
    TFile *file;
    TH2F *hStopping;
    TH2F *hStoppingAll;
    TH2F *hStoppingPrimary;
    TProfile2D *hBragg;
    TTree *tStopping;
    G4double xMin, xMax, yMin, yMax, zMin, zMax;
    const G4int nBins;
    Short_t A, Z;
    Float_t x, y, z;
    Bool_t isBetaPlus;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

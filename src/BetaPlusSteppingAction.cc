//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: BetaPlusSteppingAction.cc 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file BetaPlusSteppingAction.cc
/// \brief Implementation of the BetaPlusSteppingAction class

#include "BetaPlusSteppingAction.hh"

#include "BetaPlusDetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Track.hh"
#include "G4StepPoint.hh"
#include "G4ParticleDefinition.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaPlusSteppingAction* BetaPlusSteppingAction::fgInstance = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaPlusSteppingAction* BetaPlusSteppingAction::Instance()
{
// Static acces function via G4RunManager 

  return fgInstance;
}      

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaPlusSteppingAction::BetaPlusSteppingAction(BetaPlusRunAction *aRunAction)
: G4UserSteppingAction(),
  theRunAction(aRunAction)
{ 
  fgInstance = this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaPlusSteppingAction::~BetaPlusSteppingAction()
{ 
  fgInstance = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaPlusSteppingAction::UserSteppingAction(const G4Step* step)
{
  // fill the histograms
  G4StepPoint* prePoint  = step->GetPreStepPoint();
  G4StepPoint* postPoint = step->GetPostStepPoint();
  theRunAction->FillBragg(prePoint->GetPosition(), postPoint->GetPosition(), step->GetTotalEnergyDeposit());

  const double tinyEnergy = 1*eV;
  if(postPoint->GetKineticEnergy()<=tinyEnergy) {
    G4ThreeVector const &postPosition = postPoint->GetPosition();

    G4Track *aTrack = step->GetTrack();
    if(aTrack->GetParentID()==0)
      theRunAction->FillStoppingPrimary(postPosition);

    G4ParticleDefinition const *def = aTrack->GetParticleDefinition();
    const int Z = def->GetAtomicNumber();
    const int A = def->GetAtomicMass();
    const bool betaPlus = isBetaPlusEmitter(A,Z);
    if(betaPlus)
      theRunAction->FillStopping(postPosition);

    theRunAction->FillStoppingAll(A, Z, postPosition, betaPlus);
  }
}

bool BetaPlusSteppingAction::isBetaPlusEmitter(const int A, const int Z) {
  if((A==7 && Z==4)
     || (A==9 && Z==6)
     || (A==10 && Z==6)
     || (A==11 && Z==6)
     || (A==12 && Z==7)
     || (A==13 && Z==7)
     || (A==13 && Z==8)
     || (A==14 && Z==8)
     || (A==15 && Z==8)
     || (A==17 && Z==9)
     || (A==18 && Z==9)
     || (A==17 && Z==10)
     || (A==18 && Z==10)
     || (A==19 && Z==10)
    )
    return true;
  else
    return false;
}

//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: BetaPlusRunAction.cc 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file BetaPlusRunAction.cc
/// \brief Implementation of the BetaPlusRunAction class

#include "BetaPlusRunAction.hh"

#include "G4Run.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4UIcommand.hh"
#include "CLHEP/Units/SystemOfUnits.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaPlusRunAction::BetaPlusRunAction(const G4String &aFileStem, const int bins)
 : G4UserRunAction(),
  outputFileStem(aFileStem),
  file(NULL),
  hStopping(NULL),
  hStoppingAll(NULL),
  hStoppingPrimary(NULL),
  hBragg(NULL),
  xMin(0.), xMax(0.),
  yMin(0.), yMax(0.),
  zMin(0.), zMax(0.),
  nBins(bins),
  A(0), Z(0),
  x(0.), y(0.), z(0.),
  isBetaPlus(false)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaPlusRunAction::~BetaPlusRunAction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaPlusRunAction::BeginOfRunAction(const G4Run* aRun)
{ 
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;

  //inform the runManager to save random number seed

  G4RunManager::GetRunManager()->SetRandomNumberStore(false);

  G4LogicalVolume* lv
    = G4LogicalVolumeStore::GetInstance()->GetVolume("World");
  G4Box* box = NULL;
  if ( lv ) box = dynamic_cast<G4Box*>(lv->GetSolid());
  if ( box ) {
    xMax = box->GetXHalfLength()/CLHEP::cm;
    xMin = - xMax;
    yMax = box->GetYHalfLength()/CLHEP::cm;
    yMin = - yMax;
    zMax = box->GetZHalfLength()/CLHEP::cm;
    zMin = - zMax;
  }
  else  {
    G4cerr << "Target volume of box not found." << G4endl;
    G4cerr << "Perhaps you have changed geometry." << G4endl;
    G4cerr << "The gun will be place in the center." << G4endl;
  }

  G4String outputFileName = outputFileStem + G4UIcommand::ConvertToString(aRun->GetRunID()) + ".root";
  file = new TFile(outputFileName, "RECREATE");
  hStopping = new TH2F("hStopping","stopping density of beta+ emitters", nBins, zMin, zMax, nBins, xMin, xMax);
  hStopping->GetXaxis()->SetTitle("depth (cm)");
  hStopping->GetYaxis()->SetTitle("transverse position (cm)");
  hStopping->GetZaxis()->SetTitle("density of stopped beta^{+} emitters (1/cm^{3}/primary)");

  hStoppingAll = new TH2F("hStoppingAll","stopping density", nBins, zMin, zMax, nBins, xMin, xMax);
  hStoppingAll->GetXaxis()->SetTitle("depth (cm)");
  hStoppingAll->GetYaxis()->SetTitle("transverse position (cm)");
  hStoppingAll->GetZaxis()->SetTitle("density of all stopped particles (1/cm^{3}/primary)");

  hStoppingPrimary = new TH2F("hStoppingPrimary","stopping density of primary beam particles", nBins, zMin, zMax, nBins, xMin, xMax);
  hStoppingPrimary->GetXaxis()->SetTitle("depth (cm)");
  hStoppingPrimary->GetYaxis()->SetTitle("transverse position (cm)");
  hStoppingPrimary->GetZaxis()->SetTitle("density of stopped primary beam particles (1/cm^{3}/primary)");

  hBragg = new TProfile2D("hBragg","energy deposition", nBins, zMin, zMax, nBins, xMin, xMax);
  hBragg->GetXaxis()->SetTitle("depth (cm)");
  hBragg->GetYaxis()->SetTitle("transverse position (cm)");
  hBragg->GetZaxis()->SetTitle("energy-deposition density (MeV/cm^{3}/primary)");

  tStopping = new TTree("tStopping", "stopping sites of nuclei");
  // Branches for tStopping
  tStopping->Branch("A", &A, "A/S");
  tStopping->Branch("Z", &Z, "Z/S");
  tStopping->Branch("x", &x, "x/F");
  tStopping->Branch("y", &y, "y/F");
  tStopping->Branch("z", &z, "z/F");
  tStopping->Branch("isBetaPlus", &isBetaPlus, "isBetaPlus/O");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaPlusRunAction::EndOfRunAction(const G4Run *aRun)
{
  const int nEvents = aRun->GetNumberOfEvent();
  const double norm = ((double)(nBins*nBins))/((xMax-xMin)*(yMax-yMin)*(zMax-zMin)*((double)nEvents));

  G4cout << "Scaling histograms with norm=" << norm << G4endl;
  G4cout << " xMin=" << xMin << ", xMax=" << xMax << G4endl;
  G4cout << " yMin=" << yMin << ", yMax=" << yMax << G4endl;
  G4cout << " zMin=" << zMin << ", zMax=" << zMax << G4endl;
  G4cout << " nEvents=" << nEvents << G4endl;
  G4cout << " nBins=" << nBins << G4endl;
  hStopping->Scale(norm);
  hStoppingAll->Scale(norm);
  hStoppingPrimary->Scale(norm);
  hBragg->Scale(norm);

  G4cout << "Writing histograms and closing ROOT file" << G4endl;
  hStopping->Write();
  hStoppingAll->Write();
  hStoppingPrimary->Write();
  hBragg->Write();
  tStopping->Write();
  file->Close();
  hStopping = NULL;
  hStoppingAll = NULL;
  hStoppingPrimary = NULL;
  hBragg = NULL;
  tStopping = NULL;
  file = NULL;
}

void BetaPlusRunAction::FillStopping(const G4ThreeVector &position) {
  hStopping->Fill(position.z()/CLHEP::cm,position.x()/CLHEP::cm);
}

void BetaPlusRunAction::FillStoppingAll(const int aA, const int aZ, const G4ThreeVector &position, const bool betaPlus) {
  hStoppingAll->Fill(position.z()/CLHEP::cm,position.x()/CLHEP::cm);
  if(aA>0) {
    A=aA;
    Z=aZ;
    x=position.x()/CLHEP::cm;
    y=position.y()/CLHEP::cm;
    z=position.z()/CLHEP::cm;
    isBetaPlus=betaPlus;
    tStopping->Fill();
  }
}

void BetaPlusRunAction::FillStoppingPrimary(const G4ThreeVector &position) {
  hStoppingPrimary->Fill(position.z()/CLHEP::cm,position.x()/CLHEP::cm);
}

void BetaPlusRunAction::FillBragg(G4ThreeVector const &prePosition, G4ThreeVector const &postPosition, const double eDep) {
  G4double x1 = prePosition.x()/CLHEP::cm;
  G4double x2 = postPosition.x()/CLHEP::cm;
  G4double z1 = prePosition.z()/CLHEP::cm;
  G4double z2 = postPosition.z()/CLHEP::cm;
  G4double ax  = x1 + G4UniformRand()*(x2-x1);
  G4double az  = z1 + G4UniformRand()*(z2-z1);
  hBragg->Fill(az, ax, eDep/CLHEP::MeV);
}
